# Project presentation

<img src="https://framagit.org/ernesst/uMarks/raw/master/picture/umarks.png" width="196">

The first objective of this simple app is for me to learn how to develop an application for the mobile platform, **Ubuntu touch**,  surported by [Ubports](https://www.ubports.com) and to have my bookmarks from my personal cloud on my ubuntu touch devices.
Hopefully this project is going to be useful to you.


# Thanks, big thanks !

I would like to thanks several projects / persons
1. [Nextcloud Bookmark project](https://github.com/nextcloud/bookmarks),
1. [PyOtherSide](http://pyotherside.readthedocs.io/en/latest/) an interface between python and QML,
1. Mardy, whre my fork is coming from : [owncloud-bookmarks](https://gitlab.com/mardy/owncloud-bookmarks),
1. Joan CiberSheep for the icon using Suru Icon Theme elements
1. Dan from Dekko,
1. Ubports community.

# Features

The application is able to store localy and display the bookmarks from your nextcloud server.
The login part is handle by ubuntu touch it self and currently only the devel image support the latest Nextcloud version.
The bookmark are automatically sorted by title, if you need to order then modify the title to reflect the needed order.

Not tested on Owncloud, if anybody could give a feedback, would be great.

<img src="https://framagit.org/ernesst/uMarks/raw/master/picture/umarks-screen.png" width="480">

# How to
<img src="https://framagit.org/ernesst/uMarks/raw/master/picture/uMarks-Instruction.png" width="640">

# limitations

The application can display only one tag per address and cannot edit or delete a bookmark.

To delete the application, I advice first to remove first the online account rigth.

# Future improvment

1. Confirm it works for Owncloud,
1. Clean the code,
1. Display all the tag of a bookmark,
1. Group bookmarks per tag,
1. Edit bookmarks,
1. Delete bookmarks,
1. Create bookmarks,
1. traduction.


# Contribution

Any help on the code is welcome to enhance the app.


Ernesst
