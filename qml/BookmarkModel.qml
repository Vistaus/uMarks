/*
 * Copyright (C) 2017 Ernesst <slash.tux@gmail.com> 
 * Copyright (C) 2016 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import io.thp.pyotherside 1.4 // This will allow us to use python. We need the library in ../lib/arm-linux-gnueabihf

ListModel {
    id: root

    property string host: ""

    onHostChanged: console.log("[LOG]: Host changed to " + host)



    function login(username, password) {
        //root.clear();
        //root.clear();
        console.log("[LOG]: Logging in as " + username)
        var http = new XMLHttpRequest()
        var url = host +"/index.php/apps/bookmarks/public/rest/v2/bookmark";
        http.open("GET", url, true);
        http.setRequestHeader("Authorization", "Basic " + Qt.btoa(encodeURIComponent(username) + ":" + encodeURIComponent(password)))
        http.onreadystatechange = function() {
            if (http.readyState === 4){
                if (http.status == 200) {
                    console.log("[LOG]: http status :" + http.status)
                    //add a check on the file if written
                    var response = JSON.parse(http.responseText)
                    py.fetch_JSON();
                    parseResponse()
                    return true
                } else {
                    console.log("[error]: http status :" + http.status)
                    return false
                }
            }
        };

        http.send(null);
     }

    function parseResponse() {
        py.call("sortJSON.sort_JSON", ["title"], function() {});
        py.read_JSON();
    }
}
